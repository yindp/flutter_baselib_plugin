import 'package:flutter/material.dart';
import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:flutter_baselib/src/base/view_model/base_common_view_model.dart';
import 'package:flutter_baselib/src/const/constants.dart';

///@date:  2021/3/1 11:09
///@author:  lixu
///@description: ViewModel基类
///进入View页面时，需要http获取数据后才能显示UI的场景继承[BaseViewModel]类，配合[BaseView]使用
///泛型T：进入页面时，http获取数据对象的类型
///1.列表加载(分页加载)
///2.单个对象加载
///封装：通用加载页,集成了加载失败和加载为空的逻辑
abstract class BaseViewModel<T> extends BaseCommonViewModel {
  ///进入页面时（首次）调用接口是否成功
  bool _isLoadingSuccess = false;

  ///数据源:针对列表请求
  List<T>? dataList;

  ///数据源:针对单个对象请求
  T? dataBean;

  ///分页页数
  late int _pageNum;

  ///分页加载：能否加载更多
  bool _canLoadMore = true;

  ///是否有分页加载功能
  ///默认true
  late bool _isPageLoad;

  ///接口获取的是否是列表数据,否则就是单个数据对象
  ///默认true
  late bool _isRequestListData;

  ///下拉刷新完成提示
  String? _refreshedText = '刷新完成';

  ///每页请求数据个数,用于分页加载
  late int _pageSize;

  BaseViewModel({bool? isRequestListData, bool? isPageLoad, int? initPage, int? pageSize}) {
    _isRequestListData = isRequestListData ?? true;
    _isPageLoad = isPageLoad ?? true;
    _pageNum = initPage ?? 0;
    _pageSize = pageSize ?? Constants.pageSize;
    LogUtils.i(getTag(), '构造:${toString()}');
  }

  int get pageNum => _pageNum;

  int get pageSize => _pageSize;

  String? get refreshedText => _refreshedText;

  bool get canLoadMore => _canLoadMore;

  ///获取http请求参数
  Map<String, dynamic> getRequestParams();

  ///获取http请求url
  String getUrl();

  ///请求失败是否显示Toast提示错误信息
  ///子类根据需求重写
  ///[isLoadMore] 是否是加载更多时，回调的该方法
  bool isShowFailToast(bool isLoadMore) {
    return true;
  }

  ///分页加载完所有数据时回调
  ///子类根据需求重写
  void onLoadCompleteAll() {
    ToastUtils.show('已加载完全部');
  }

  ///请求失败回调,子类根据需求重写
  ///[errorBean] http失败响应对象
  ///[isLoadMoreFail] 是否是加载更多失败
  void onErrorCallback(HttpErrorBean errorBean, {bool isLoadMoreFail = false}) {}

  void setCanLoadMore(bool value) {
    _canLoadMore = value;
  }

  ///加载数据
  ///第一次请求或下拉刷新时调用(请求参数设置为初始值)
  @override
  Future onLoading(BuildContext context) async {
    LogUtils.i(getTag(), 'onLoading');

    if (isLoading) {
      LogUtils.w(getTag(), 'onLoading() is Loading');
      return;
    }

    isLoading = true;
    _refreshedText = '刷新成功';

    CancelToken cancelToken = CancelToken();
    cancelTokenList.add(cancelToken);

    ///请求失败回调
    var onError = (HttpErrorBean errorBean) {
      _isLoadingSuccess = false;
      _refreshedText = '刷新失败';
      onErrorCallback(errorBean);
    };

    ///请求完成回调
    var onComplete = () {
      isLoading = false;
      cancelTokenList.remove(cancelToken);
    };

    if (_isRequestListData) {
      ///请求列表数据
      await api.requestList<T>(
        getUrl(),
        params: getRequestParams(),
        isShowLoading: false,
        isShowFailToast: isShowFailToast(false),
        cancelToken: cancelToken,
        onError: onError,
        onComplete: onComplete,
        onSuccess: (List<T>? list) {
          ///请求成功回调
          _isLoadingSuccess = true;
          dataList = [];
          list = list ?? [];

          ///解析json
          dataList!.addAll(list);

          if (_isPageLoad) {
            ///list分页加载
            if (list.length < _pageSize) {
              _canLoadMore = false;
            } else {
              _canLoadMore = true;
              _pageNum++;
            }
          } else {
            ///list没有分页加载
            _canLoadMore = false;
          }
        },
      );
    } else {
      ///请求单个数据对象
      await api.request<T>(
        getUrl(),
        params: getRequestParams(),
        isShowLoading: false,
        isShowFailToast: isShowFailToast(false),
        cancelToken: cancelToken,
        onError: onError,
        onComplete: onComplete,
        onSuccess: (T? bean) {
          ///请求成功回调
          _isLoadingSuccess = true;
          dataBean = bean;
        },
      );
    }
  }

  ///加载更多:针对分页加载
  Future onLoadingMore(context) async {
    LogUtils.i(getTag(), 'onLoadMore:$_canLoadMore');
    if (_canLoadMore) {
      if (isLoading) {
        LogUtils.w(getTag(), 'onLoadMore is Loading');
        return;
      }

      isLoading = true;
      CancelToken cancelToken = CancelToken();
      cancelTokenList.add(cancelToken);

      await api.requestList<T>(
        getUrl(),
        params: getRequestParams(),
        isShowLoading: false,
        isShowFailToast: isShowFailToast(true),
        onSuccess: (List<T>? list) {
          ///请求成功回调
          if (list != null) {
            dataList ??= [];
            dataList?.addAll(list);

            if (list.length < Constants.pageSize) {
              _canLoadMore = false;
            } else {
              _canLoadMore = true;
              _pageNum++;
            }
          }
        },
        onError: (HttpErrorBean errorBean) {
          ///请求失败回调
          onErrorCallback(errorBean, isLoadMoreFail: true);
        },
        onComplete: () {
          ///请求完成回调
          isLoading = false;
          cancelTokenList.remove(cancelToken);
        },
      );
      notifyListeners();
    } else {
      onLoadCompleteAll();
    }
  }

  ///手动下拉刷新
  Future onPullToRefresh(BuildContext context) async {
    await onLoading(context);
    notifyListeners();
  }

  ///请求是否成功
  @override
  bool isSuccess() {
    if (_isLoadingSuccess) {
      if (_isRequestListData) {
        return dataList != null && dataList!.length > 0;
      } else {
        return dataBean != null;
      }
    } else {
      return false;
    }
  }

  ///请求是否失败
  @override
  bool isFail() {
    return !_isLoadingSuccess;
  }

  ///请求数据是否为空
  @override
  bool isEmpty() {
    if (_isRequestListData) {
      return dataList == null || dataList!.isEmpty;
    } else {
      return dataBean == null;
    }
  }
}
