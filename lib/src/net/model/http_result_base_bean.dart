import 'package:flutter_baselib/flutter_baselib.dart';

///@date:  2021/2/25 14:22
///@author:  lixu
///@description: 网络请求响应基类
abstract class HttpResultBaseBean {
  ///响应码
  String code;

  ///错误提示
  String? message;

  HttpResultBaseBean({required this.code, this.message});

  ///是否请求成功
  bool isSuccess() {
    return HttpCode.success == code;
  }
}
