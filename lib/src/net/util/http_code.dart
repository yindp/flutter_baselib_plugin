///@date:  2021/2/25
///@author:  lixu
///@description: 网络请求内置错误码
class HttpCode {
  HttpCode._();

  ///请求成功
  static String success = '0';

  ///请求失败
  static String fail = '-1';

  ///网络错误
  static String networkError = '-2';

  ///网络返回数据格式化异常
  static String jsonParseException = '-3';

  static String connectTimeout = '-4';

  static String sendTimeout = '-5';

  static String receiveTimeout = '-6';

  static String cancel = '-8';

  static String unKnowError = '-9';

  static String defaultCode = '-10';
}
