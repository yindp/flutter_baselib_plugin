import 'dart:io';

import 'package:flutter_baselib/flutter_baselib.dart';

///@date:  2021/2/25 14:22
///@author:  lixu
///@description: 全局http请求相关配置
abstract class IHttpConfig {
  ///配置默认值：http请求时是否显示加载dialog
  bool isShowLoading();

  ///配置默认值：http加载提示文本
  String configLoadingText();

  ///配置默认值：加载中能否通过关闭加载弹窗取消请求
  bool isCancelableDialog();

  ///配置默认值：请求失败时是否自动显示toast提示错误
  bool isShowFailToast();

  ///配置默认值：请求前是否校验网络连接
  ///true：如果无网络，直接返回错误
  bool isCheckNetwork();

  ///配置通用的http请求选项[BaseOptions]
  ///优先级最低，优先取[XApi]#[request]方法中配置的method和option
  BaseOptions configBaseOptions();

  ///返回http成功的响应码
  String configHttpResultSuccessCode();

  ///配置https
  bool configHttps(X509Certificate cert, String host, int port);

  ///添加拦截器
  ///拦截器队列的执行顺序是FIFO，先添加的拦截器先执行
  List<Interceptor>? configInterceptors();

  ///是否自动添加[LogInterceptors]默认日志拦截器,打印http请求响应相关的日志
  bool configLogEnable();

  ///如果url中不包含baseUrl，请求前回调该方法获取baseUrl
  ///优先级高于[IHttpConfig]#[configBaseOptions]方法配置的baseUrl
  ///[url] 当前正在请求的接口url
  ///return: 返回null使用[IHttpConfig]#[configBaseOptions]方法配置的baseUrl
  Future<String?> getBaseUrl(String url);

  ///http请求失败时会回调该方法，判断是否是token失效导致的错误
  ///[errorBean] 请求失败对象
  Future<bool> isHttpRespTokenError(HttpErrorBean errorBean);

  ///token失效回调该方法
  ///[errorBean] 请求失败对象
  Future<void> onTokenErrorCallback(HttpErrorBean errorBean);

  ///将http响应的json解析成对象
  ///[url] 当前请求url
  ///[jsonData] http响应完整json
  ///[isRespListData] 响应数据是否是List格式
  Future<HttpResultBean<T>> parseJsonToObject<T>(String url, Map<String, dynamic> jsonData, bool isRespListData);

  ///http请求显示加载框：[XApi]#[request]方法isShowLoading字段为true时，会回调该方法
  ///[url] 当前请求url
  ///[tag] 当前请求对应的tag，唯一
  ///[cancelToken] 用于加载框关闭时取消http请求
  ///[loadingText] 加载提示提示
  ///[isCancelableDialog] 请求过程中能否关闭加载框,默认false
  void showLoading(String url, int tag, CancelToken cancelToken, String loadingText, bool isCancelableDialog);

  ///请求完成，关闭加载框：[XApi]#[request]方法isShowLoading字段为true时，会回调该方法
  ///[url] 当前请求url
  ///[tag]当前请求对应的tag，唯一
  ///[isCancelled]当前请求是否已经取消，如果已经取消则不用关闭dialog
  void hideLoading(String url, int tag, bool isCancelled);
}
