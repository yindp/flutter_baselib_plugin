///@date:  2021/3/1 11:44
///@author:  lixu
///@description: 常量类
class Constants {
  static const pageSize = 20;
  static const httpStartWith = 'http';
}
