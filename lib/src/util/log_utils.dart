import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:flutter_xlog_plugin/xlog_lib.dart';

///@date:  2021/02/24
///@author:  lixu
///@description:  日志打印
class LogUtils {
  LogUtils._();

  ///日志工具初始化
  static Future<void> init(LogConfig logConfig) {
    return XLogUtils.init(
      tag: logConfig.tag,
      isConsolePrintLog: logConfig.isConsolePrintLog,
      isUseFlutterPrintMethodLog: logConfig.isFlutterPrintLog,
      saveLogFilePath: logConfig.saveLogFilePath,
      isLogFileEncrypt: logConfig.isLogFileEncrypt,
    );
  }

  static void v(String tag, String? msg) {
    XLogUtils.v(tag, msg);
  }

  static void d(String tag, String? msg) {
    XLogUtils.d(tag, msg);
  }

  static void i(String tag, String? msg) {
    XLogUtils.i(tag, msg);
  }

  static void w(String tag, String? msg) {
    XLogUtils.w(tag, msg);
  }

  static void e(String tag, String? msg) {
    XLogUtils.e(tag, msg);
  }

  static Future<void> dispose() async {
    await XLogUtils.dispose();
  }
}
