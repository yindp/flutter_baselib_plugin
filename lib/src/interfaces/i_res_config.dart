import 'package:flutter/material.dart';

///@date:  2021/3/1 14:03
///@author:  lixu
///@description: 全局通用资源配置
abstract class IResConfig {
  ///配置全局http加载中的页面
  Widget? configLoadingView();

  ///配置全局http加载数据为空的页面
  Widget? configLoadEmptyView();

  ///配置全局http加载失败的页面
  Widget? configLoadErrorView();
}
