#import "FlutterBaselibPlugin.h"
#if __has_include(<flutter_baselib/flutter_baselib-Swift.h>)
#import <flutter_baselib/flutter_baselib-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "flutter_baselib-Swift.h"
#endif

@implementation FlutterBaselibPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFlutterBaselibPlugin registerWithRegistrar:registrar];
}
@end
