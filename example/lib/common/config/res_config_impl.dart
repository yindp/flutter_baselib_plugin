import 'package:flutter/material.dart';
import 'package:flutter_baselib/flutter_baselib.dart';

import 'loadview/load_empty_view.dart';
import 'loadview/load_error_view.dart';
import 'loadview/loading_view.dart';

///@date:  2021/3/1 14:06
///@author:  lixu
///@description:  全局资源配置
///1、配置http通用加载错误页，空白页以及正常显示页UI和逻辑，UI可自定义；
///TODO 下面定义的是全局资源，可以针对具体页面，使用[BaseView]自定义UI样式；
class ResConfigImpl implements IResConfig {
  ///自定义http全局空白页
  @override
  Widget? configLoadEmptyView() {
    return LoadEmptyView();
  }

  ///自定义http全局加载错误页
  @override
  Widget? configLoadErrorView() {
    return LoadErrorView();
  }

  ///自定义http全局加载中的页面
  @override
  Widget? configLoadingView() {
    return LoadingView();
  }
}
