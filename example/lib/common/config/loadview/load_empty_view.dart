import 'package:flutter/material.dart';

///@date:  2021/3/1 13:48
///@author:  lixu
///@description: 加载数据为空view
class LoadEmptyView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Text(
          '~数据为空\n点击重新加载',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 16,
            color: const Color(0xFFADADAD),
          ),
        ),
      ),
    );
  }
}
