import 'package:flutter/material.dart';

///@date:  2021/3/11 11:23
///@author:  lixu
///@description:UI 工具类
class UIUtils {
  UIUtils._();

  ///获取通用演示按钮
  static Widget getButton(String text, VoidCallback? onPressed) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: RaisedButton(
        textColor: Colors.white,
        color: Colors.blue,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 12),
          child: Text(
            text,
            textAlign: TextAlign.center,
          ),
        ),
        onPressed: onPressed,
      ),
    );
  }

  ///登录页面演示功能说明Widget
  static Widget getLoginPageDemoDescWidget() {
    String demoDesc = '登录页演示功能：lib\\module\\login包\n'
        '1、基于dio封装的网络请求组件XApi\n'
        '2、http请求时传递泛型直接解析为对应对象或List\n'
        '3、XApi.request()和requestList()方法使用\n'
        '4、http加载时dialog自动显示隐藏逻辑，dialog UI可自定义\n'
        '5、http加载时手动关闭dialog时，自动取消请求\n'
        '6、MVVM：BaseCommonViewModel使用场景\n'
        '7、MVVM：ViewModel监听页面生命周期，页面关闭时自动取消请求\n';
    return Container(
      margin: EdgeInsets.only(bottom: 20),
      child: Text(
        demoDesc,
        style: TextStyle(fontSize: 14),
      ),
    );
  }

  ///用户列表页面2演示功能说明Widget
  static Widget getUserPage1DemoDescWidget() {
    String demoDesc = '用户页演示功能：lib\\module\\userlist包\n'
        '1、进入页面时调用1个接口获取数据成功后，才能显示UI\n'
        '2、请求过程中，页面关闭ViewModel自动取消请求\n'
        '3、Http通用加载错误页，空白页以及正常显示页UI和逻辑，UI可自定义\n'
        '4、MVVM：BaseViewModel和BaseView使用场景\n';

    return Container(
      margin: EdgeInsets.only(bottom: 20),
      child: Text(
        demoDesc,
        style: TextStyle(fontSize: 14),
      ),
    );
  }

  ///用户列表页面2演示功能说明Widget
  static Widget getUserPage2DemoDescWidget() {
    String demoDesc = '用户页演示功能：lib\\module\\userlist包\n'
        '与UserPage1演示功能一样，只是多了下面功能：\n'
        '1、进入页面时调用2（多）个接口获取数据成功后，才能显示UI\n\n'
        '建议：\n针对上面的场景，对每个接口封装独立的ViewModel来处理逻辑，类似demo中的这种方式\n';

    return Container(
      margin: EdgeInsets.only(bottom: 20),
      child: Text(
        demoDesc,
        style: TextStyle(fontSize: 14),
      ),
    );
  }
}
