///@date:  2021/2/26 18:10
///@author:  lixu
///@description:
class HttpConst {
  ///http超时
  static const httpTimeOut = 10 * 1000;

  ///请求网络的状态code
  static const httpResultSuccess = 200;

  ///token错误code
  static const sysTokenError = 900001;

  ///token过期code
  static const sysTokenExpired = 900002;

  static const serverKey = '9vFLsTux4GN5dGFuDMhL';
}
