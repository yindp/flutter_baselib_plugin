import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:flutter_baselib_example/common/net/bean_factory.dart';

///@date:  2021/2/26 21:19
///@author:  lixu
///@description: http json解析工具
class HttpJsonUtils {
  static String _tag = 'HttpJsonUtils';

  ///TODO 参考下面代码逻辑，根据业务修改代码解析json为object
  ///TODO 最终就是要将json转换为Object或List并封装到HttpResultBean对象中

  ///http响应json解析成对象
  ///[jsonData] http响应完整json
  ///[isRespListData] 响应数据是否是List格式
  static HttpResultBean<T> parseJsonToObject<T>(String url, Map<String, dynamic> jsonData, bool isRespListData) {
    HttpResultBean<T> resultBean = HttpResultBean();
    resultBean.isRespListData = isRespListData;

    try {
      resultBean.code = jsonData['code']?.toString() ?? HttpCode.defaultCode;
      resultBean.message = jsonData['message'];

      if (isRespListData) {
        ///响应的是list
        resultBean.dataList = _parseToListObject(url, jsonData, resultBean.code);
      } else {
        ///响应的是object
        resultBean.data = _parseToObject(url, jsonData, resultBean.code);
      }
    } catch (exception) {
      LogUtils.e(_tag, exception.toString());

      if (resultBean.code == HttpCode.defaultCode || resultBean.code == HttpCode.success) {
        if (resultBean.code == HttpCode.success) {
          resultBean.message = exception.toString();
        }
        resultBean.code = HttpCode.jsonParseException;
      }
    }

    return resultBean;
  }

  ///json解析为list
  ///[jsonData] json数据
  ///[code] http响应码
  static List<T> _parseToListObject<T>(String url, Map<String, dynamic>? jsonData, String code) {
    var body = jsonData?['data'];

    List<T> listData = [];

    if (HttpCode.success == code && body == null) {
      ///请求成功,响应无data,不用解析
    } else {
      if (HttpCode.success == code && body != null && body is List) {
        body.forEach((value) {
          T? bean = BeanFactory.parseObject<T>(value);
          if (bean != null) {
            listData.add(bean);
          }
        });
      }
    }
    return listData;
  }

  ///json解析为object
  ///[jsonData] json数据
  ///[code] http响应码
  static T? _parseToObject<T>(String url, Map<String, dynamic>? jsonData, String code) {
    if (HttpCode.success == code && jsonData == null) {
      ///请求成功,响应无data,不用解析
      return null;
    } else {
      return BeanFactory.parseObject<T>(jsonData);
    }
  }
}
