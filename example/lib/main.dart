import 'package:flutter/material.dart';
import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:flutter_baselib_example/module/home/view/home_page.dart';

void main() {
  FlutterBugly.postCatchException(
    () => runApp(App()),
    onCatchCallback: (String errMsg, errDetail) {
      ///上传日志到bugly时，回调该方法
      LogUtils.e('bugly捕获异常msg', errMsg);
      LogUtils.e('bugly捕获异常detail', errDetail);

      ///TODO 根据业务处理逻辑
    },
  );
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomePage(),
      //初始化全局context
      builder: BaseLibPlugin.oneContextBuilder,
      navigatorKey: BaseLibPlugin.oneContextKey,
    );
  }
}
