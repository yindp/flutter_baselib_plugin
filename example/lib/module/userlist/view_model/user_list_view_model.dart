import 'package:flutter/material.dart';
import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:flutter_baselib_example/common/config/login_info_manager.dart';
import 'package:flutter_baselib_example/common/net/http_urls.dart';
import 'package:flutter_baselib_example/module/login/model/login_result_bean.dart';
import 'package:flutter_baselib_example/module/userlist/model/user_detail_bean.dart';
import 'package:flutter_baselib_example/module/userlist/view_model/token_view_model.dart';

///@date:  2021/3/2 11:26
///@author:  lixu
///@description: 用户列表viewModel
///进入View页面时，需要http获取数据后才能显示UI的场景继承[BaseViewModel]类，配合[BaseView]使用
///泛型[UserDetailBean]：进入页面时，http获取数据对象的类型
class UserListViewModel extends BaseViewModel<UserDetailBean> {
  UserListViewModel() : super(isPageLoad: false);

  ///TODO 第一个接口的响应作为第二个接口的参数
  LoginResultBean? loginResultBean;

  ///TODO 此处演示第二个接口需要依赖第一个接口返回的参数
  ///参考：https://gitee.com/rishli/flutter_baselib_plugin/issues/I3CLN3
  @override
  Future onLoading(BuildContext context) async {
    ///获取第一个接口的返回值，作为第二个接口的参数：

    try {
      loginResultBean = context.read<TokenViewModel>().dataBean;
    } catch (e) {
      ///TODO UserListViewModel多个页面复用，在未使用TokenViewModel的页面会异常，此处捕获异常，适配下
      LogUtils.e(getTag(), e.toString());
    }

    return super.onLoading(context);
  }

  ///获取http请求参数
  @override
  Map<String, dynamic> getRequestParams() {
    return {
      'userId': loginInfo.userBean?.userId,

      ///TODO 第一个接口的响应作为第二个接口的参数
      'token': loginResultBean?.token ?? loginInfo.token,
    };
  }

  @override
  String getTag() {
    return 'UserListViewModel';
  }

  ///获取http请求url
  @override
  String getUrl() {
    return HttpUrls.userListUrl;
  }

  ///TODO 下面重写的方法只是为了演示：进入页面时请求http失败时，http通用加载错误页，空白页以及正常显示页UI和逻辑
  ///
  ///TODO 重试次数，演示进入页面时请求http失败的场景
  int retryCount = 0;

  ///请求是否失败
  ///TODO 只是为了演示进入页面时请求http失败的场景
  @override
  bool isFail() {
    if (retryCount == 0) {
      retryCount++;
      dataBean = null;
      dataList = null;
      return true;
    } else {
      return super.isFail();
    }
  }

  ///请求是否为空
  ///TODO 只是为了演示进入页面时请求http为空的场景
  @override
  bool isEmpty() {
    if (retryCount == 1) {
      retryCount++;
      dataBean = null;
      dataList = [];
      return true;
    } else {
      return super.isEmpty();
    }
  }
}
