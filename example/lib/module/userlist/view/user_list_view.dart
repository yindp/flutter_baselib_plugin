import 'package:flutter/material.dart';
import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:flutter_baselib_example/module/userlist/view/item/user_item.dart';
import 'package:flutter_baselib_example/module/userlist/view_model/user_list_view_model.dart';

///@date:  2021/3/2 11:29
///@author:  lixu
///@description:用户列表ListView
class UserListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<UserListViewModel>(builder: (context, viewModel, child) {
      return ListView.separated(
          key: PageStorageKey(1),
          padding: EdgeInsets.symmetric(horizontal: 20),
          separatorBuilder: (BuildContext context, int index) {
            return Divider(
              height: 0.5,
              color: Colors.grey[400],
            );
          },
          itemBuilder: (context, index) {
            return UserItem(viewModel.dataList![index]);
          },
          itemCount: viewModel.dataList!.length);
    });
  }
}
