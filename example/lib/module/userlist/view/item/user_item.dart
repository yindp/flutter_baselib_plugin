import 'package:flutter/material.dart';
import 'package:flutter_baselib_example/module/userlist/model/user_detail_bean.dart';

///@date:  2021/3/2 11:33
///@author:  lixu
///@description: 用户List item
class UserItem extends StatelessWidget {
  final UserDetailBean _userDetailBean;

  UserItem(this._userDetailBean);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      height: 70,
      child: Text(
        _userDetailBean.name ?? 'unknow',
        style: TextStyle(fontSize: 18, color: Colors.black38),
      ),
    );
  }
}
