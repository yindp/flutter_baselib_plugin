import 'package:flutter/material.dart';
import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:flutter_baselib_example/common/util/ui_utils.dart';
import 'package:flutter_baselib_example/module/userlist/view/user_list_view.dart';
import 'package:flutter_baselib_example/module/userlist/view_model/user_list_view_model.dart';

///@date:  2021/3/11
///@author:  lixu
///@description:用户列表页面1
///TODO 进入页面时调用1个接口获取数据成功后，才能显示UI
///TODO  1、用户列表接口
class UserPage1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('用户列表页面UserPage1(MVVM)')),
      body: ChangeNotifierProvider(
        create: (_) {
          return UserListViewModel();
        },
        child: Column(
          children: [
            UIUtils.getUserPage1DemoDescWidget(),
            Divider(color: Colors.grey, height: 1),
            Expanded(
              child: BaseView<UserListViewModel>(
                child: UserListView(),
                errorChild: Center(
                  child: Text(
                    '这是自定义请求失败的页面：\n请求失败，点击重试',
                    style: TextStyle(color: Colors.red, fontSize: 20),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
