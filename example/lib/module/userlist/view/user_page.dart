import 'package:flutter/material.dart';
import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:flutter_baselib_example/module/userlist/view/user_list_view.dart';
import 'package:flutter_baselib_example/module/userlist/view_model/user_list_view_model.dart';

///@date:  2021/3/11
///@author:  lixu
///@description:用户列表页面
///TODO 进入页面时调用接口获取数据成功后，才能显示UI
class UserPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('用户列表页面UserPage(MVVM)')),
      body: ChangeNotifierProvider(
        create: (_) {
          return UserListViewModel();
        },
        child: BaseView<UserListViewModel>(
          child: UserListView(),
          errorChild: Center(
            child: Text(
              '这是自定义请求失败的页面：\n请求失败，点击重试',
              style: TextStyle(color: Colors.red, fontSize: 20),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
    );
  }
}
