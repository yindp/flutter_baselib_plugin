## 一、flutter_baselib 简介
[技术文档参考](https://juejin.cn/post/6936911560728281102)  


基于MVVM架构封装Flutter基础库，包含：  
1、xlog日志库；  
2、bugly异常上报、应用更新；  
3、基于dio封装网络请求基类；  
4、封装http通用加载错误页，空白页以及正常显示页UI和相关逻辑，UI可自定义；  
5、封装http通用加载dialog控件和dialog显示隐藏逻辑，dialog UI可自定义；  
6、基于provider封装ViewModel基类和BaseView(MVVM)；  
7、ViewModel监听页面生命周期，页面关闭时销毁资源；  
8、常用工具类；  

---

TODO:  
1、http缓存逻辑；  
2、根据具体业务场景，优化http封装逻辑；  
4、完善工具类；  
3、将不定期进行优化升级；  

---
## 二、基础库说明
集成了下面三方库，进行二次封装使用：  
1、[dio](https://pub.dev/packages/dio)：网络请求；  
2、[fluttertoast](https://pub.dev/packages/fluttertoast)：toast工具；  
3、[provider](https://pub.dev/packages/provider)：状态管理库，使用Provider来实现MVVM模式；  
4、[fluro](https://pub.dev/packages/fluro)：路由管理；  
5、[shared_preferences](https://pub.dev/packages/shared_preferences)：sp工具类  
6、[permission_handler](https://pub.dev/packages/permission_handler)：动态权限申请  
7、[connectivity](https://pub.dev/packages/connectivity)：网络连接状态工具  
8、[device_info](https://pub.dev/packages/device_info)：设备信息获取工具  
9、[package_info](https://pub.dev/packages/package_info)：包信息获取工具  
10、[path_provider](https://pub.dev/packages/path_provider)：存储路径获取  
11、[one_context](https://pub.dev/packages/one_context)：全局context，实现原理 [参考这里](https://xie.infoq.cn/article/d384d3200f010a5a66e3c485f)；

---
native 库：  
1、[bugly](https://bugly.qq.com/v2/index)：异常上报，应用升级（仅支持Android平台）；  
2、[mars xlog](https://github.com/Tencent/mars/wiki)：日志打印存储（仅Android会保存为日志文件，其它平台使用Flutter print()方法打印log到控制台）；  


## 三、使用方式
```
dependencies:
    flutter_baselib:
        git:
            url: https://gitee.com/rishli/flutter_baselib_plugin.git
            ref: (tag标签)
```

## 四、总结
    1、这些功能都比较简单，但却很常见，又是很容易忽略的一些细节，通过封装能让这些功能复用，不用关注这些细节，专注业务开发；   
    2、基础库包含的功能模块只是根据自身业务封装，不可能满足所有的场景，但是可以根据具体业务场景进行扩展，也可以借鉴这些封装思路，实现适合自身业务的基础库；   
    3、后面会完善基础库功能，扩展通用性；   